### -*-Makefile-*- pour préparer le paquetage formation-latex-ul
##
## Copyright (C) 2015-2023 Vincent Goulet
##
## 'make pdf' (recette par défaut) compile les documents auxiliaires,
## puis le document principal et les diapositives avec XeLaTeX.
##
## 'make update-copyright' met à jour l'année de copyright dans toutes
## les sources du document
##
## 'make zip' crée l'archive du paquetage conformément aux exigences
## de CTAN.
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .zip. Il n'y a pas de lien vers cette version dans la
## page web.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet formation-latex-ul
## https://gitlab.com/vigou3/formation-latex-ul

## Nom du paquetage sur CTAN
PACKAGENAME = formation-latex-ul

## Principaux fichiers
MASTER = ${PACKAGENAME}.pdf
MASTERDIAPOS = ${PACKAGENAME}-diapos.pdf
ARCHIVE = ${PACKAGENAME}.zip
README = README.md
NEWS = NEWS
LICENSE = LICENSE
CONTRIBUTING = CONTRIBUTING.md

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep "\\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Sources du document principal
SOURCESMAIN = $(filter-out ${MASTER:.pdf=.tex} \
	                   $(wildcard *-diapos.tex) \
	                   $(wildcard solutions-*.tex),\
		           $(wildcard *.tex)) \
	      ${MASTER:.pdf=.bib}

## Sources des diapositives
SOURCESDIAPOS = $(wildcard *-diapos.tex)

## Documents auxiliaires insérés dans le document principal (et donc à
## compiler avant)
AUXDOC = $(addsuffix .pdf,$(basename $(wildcard auxdoc/*.tex)))

## Fichiers de code source (aucun traitement préalable)
LISTINGS = $(wildcard listings/*)

## Fichiers des exercices (sans les solutions)
EXERCICES = $(filter-out $(wildcard exercices/*-solution*.tex),$(wildcard exercices/*.tex)) \
	    exercices/exercices.bib exercices/console-screenshot.pdf

## Images
IMAGES = $(wildcard images/*)

# Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
CP = cp -pR
RM = rm -r
MD = mkdir -p
ZIP = zip --filesync -r -9

## Dossier temporaire pour construire l'archive
BUILDDIR = tmpdir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variable automatique
TAGNAME = v${VERSION}


all: pdf

${AUXDOC}:
	cd auxdoc && \
	${TEXI2DVI} $(notdir ${AUXDOC:.pdf=.tex})

${MASTER}: ${MASTER:.pdf=.tex} ${SOURCESMAIN} \
	   ${AUXDOC} ${LISTINGS} ${EXERCICES} ${IMAGES}
	${TEXI2DVI} ${MASTER:.pdf=.tex}

${MASTERDIAPOS}: ${MASTERDIAPOS:.pdf=.tex} ${SOURCESDIAPOS} \
	         ${AUXDOC} ${EXERCICES} ${IMAGES}
	${TEXI2DVI} ${MASTERDIAPOS:.pdf=.tex}

.PHONY: pdf
pdf: ${MASTER} ${MASTERDIAPOS}

.PHONY: release
release: update-copyright zip check-status create-release upload create-link publish

.PHONY: update-copyright
update-copyright: ${MASTER:.pdf=.tex} ${SOURCESMAIN} ${SOURCESDIAPOS}
	for f in $?; \
	    do sed -E '/^(#|%)* +Copyright \(C\)/s/-20[0-9]{2}/-$(shell date "+%Y")/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: zip
zip: ${MASTER} ${MASTERDIAPOS} ${README} ${NEWS} ${LICENSE} ${CONTRIBUTING}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	${MD} ${BUILDDIR}/${PACKAGENAME}/source/auxdoc \
	      ${BUILDDIR}/${PACKAGENAME}/source/listings \
	      ${BUILDDIR}/${PACKAGENAME}/source/images \
	      ${BUILDDIR}/${PACKAGENAME}/doc
	touch ${BUILDDIR}/${PACKAGENAME}/${README} && \
	  awk '(state == 0) && /^# / { state = 1 }; \
	       /^## (Author|Auteur)/ { printf("## Version\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${PACKAGENAME}/${README}
	${CP} ${NEWS} ${LICENSE} ${CONTRIBUTING} ${BUILDDIR}/${PACKAGENAME}
	${CP} ${MASTER:.pdf=.tex} \
	      ${SOURCESMAIN} \
	      ${MASTERDIAPOS:.pdf=.tex} \
	      ${SOURCESDIAPOS} ${BUILDDIR}/${PACKAGENAME}/source
	${CP} ${AUXDOC} \
	      ${AUXDOC:.pdf=.tex} ${BUILDDIR}/${PACKAGENAME}/source/auxdoc
	${CP} ${LISTINGS} ${BUILDDIR}/${PACKAGENAME}/source/listings
	${CP} ${IMAGES} ${BUILDDIR}/${PACKAGENAME}/source/images
	${CP} ${MASTER} \
	      ${MASTERDIAPOS} \
	      ${EXERCICES} ${BUILDDIR}/${PACKAGENAME}/doc
	cd ${BUILDDIR} && ${ZIP} ../${ARCHIVE} ${PACKAGENAME}/*
	${RM} ${BUILDDIR}

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "checking the status of the local repository... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "uncommitted changes in repository; not creating release"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "vérification que la version existe déjà... "; \
	    http_code=$$(curl -I "${APIURL}/releases/${TAGNAME}" 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "oui"; \
	        printf "%s\n" "-> utilisation de la version actuelle"; \
	    else \
	        printf "%s\n" "non"; \
	        printf "%s" "création d'une version dans GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print "Édition", $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "uploading the archives to the package registry..."
	curl --upload-file ${ARCHIVE} \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/${REPOSNAME}/${TAGNAME}/${ARCHIVE}"
	@printf "\n%s\n" "ok"

.PHONY: create-link
create-link:
	@printf "%s\n" "adding assets to the release..."
	$(eval PKG_ID=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${TAGNAME}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	@for f in ${ARCHIVE}; \
	do \
	    file_id=$$(curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                    --silent \
	                    "${APIURL}/packages/${PKG_ID}/package_files" \
	               | grep -o -E "\{[^{]*\"file_name\":\"$${f}\"[^}]*}" \
	               | grep -o '"id":[0-9]*' | cut -d: -f 2) && \
	    url="${REPOSURL:/=}/-/package_files/$${file_id}/download" && \
	    printf "  url to %s: %s\n" "$${f}" "$${url}" && \
	    curl --request POST \
	         --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	         --data name="$${f}" \
	         --data url="${REPOSURL:/=}/-/package_files/$${file_id}/download" \
	         --data link_type="package" \
	         --output /dev/null --silent \
	         "${APIURL}/releases/${TAGNAME}/assets/links"; \
	done
	@printf "%s\n" "ok"

.PHONY: publish
publish:
	@printf "%s\n" "mise à jour de la page web..."
	git switch pages && \
	  ${MAKE} && \
	  git switch master
	@printf "%s\n" "ok"

.PHONY: check-url
check-url: ${SOURCESMAIN} ${SOURCESDIAPOS}
	@printf "%s\n" "vérification des adresses URL dans les fichiers source"
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq))
	@for u in ${url}; do \
	    printf "%s... " "$$u"; \
	    if curl --output /dev/null --silent --head --fail --max-time 5 "$$u"; then \
	        printf "%s\n" "ok"; \
	    else \
		printf "%s\n" "invalide ou ne répond pas"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      ${ARCHIVE} \
	      solutions-* \
	      *.aux *.log *.blg *.bbl *.out *.ilg *.idx *.ind

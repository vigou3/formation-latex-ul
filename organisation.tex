%%% Copyright (C) 2015-2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Rédaction avec LaTeX»
%%% https://gitlab.com/vigou3/formation-latex-ul
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter{Organisation d'un document}
\label{chap:organisation}

La maitrise des notions du chapitre précédent permet déjà de composer
un document simple avec {\LaTeX}. Toutefois, la puissance du système
de mise en page se manifeste vraiment lors de la préparation de
documents élaborés comportant plusieurs divisions internes, une table
des matières, des renvois, etc. Le présent chapitre aborde ces aspects
d'organisation d'un document.

\section{Choix d'une classe}
\label{sec:organisation:classe}

La première chose à faire au moment de se lancer dans la rédaction
d'un document avec {\LaTeX} consiste normalement à choisir une classe.
Vous avez déjà appris à la \autoref{sec:bases:classes} comment spécifier
la classe à utiliser. Cette section présente les différences entre les
classes ainsi que les principales options disponibles.

Les classes standards sont \class{article}, \class{report},
\class{book}, \class{letter} et \class{slides}.

\begin{description}
\item[\normalfont\class{article}] Articles scientifiques et autres
  documents de longueur modérée ne nécessitant pas une mise en page
  élaborée. Le folio (numéro de page) est placé au centre du pied de
  page. Le titre apparait dans le haut de la première page,
  immédiatement suivi du texte.
\item[\normalfont\class{report}] Rapports et autres documents plus
  longs pouvant être divisés en chapitres. Le titre apparait sur une
  page de titre. La mise en page est autrement identique à celle de la
  classe \class{article}.
\item[\normalfont\class{book}] Longs documents divisés en chapitres.
  La mise en page est conçue pour une impression recto verso. L'entête
  de la page (autre que la première du chapitre) contient le folio sur
  le bord extérieur et le titre de chapitre (page paire) ou le titre
  de section (page impaire). Le titre apparait sur une page de titre.
\item[\normalfont\class{letter}] Lettres et correspondance. Bien que
  puissante, cette classe est plus rarement utilisée. Je n'en traite
  pas davantage dans ce document.
\item[\normalfont\class{slides}] Diapositives simples pour des
  présentations. La \autoref{sec:trucs:diapositives} traite plus en
  détail de la production de diapositives.
\end{description}

Le \autoref{tab:organisation:classes} fournit un sommaire des
principales caractéristiques des classes \class{article},
\class{report} et \class{book}. De plus, les figures
\ref{fig:organisation:classes:article}--\ref{fig:organisation:classes:book}
fournissent des exemples de mise en page pour ces trois classes.

\begin{table}
  \caption{Caractéristiques des principales classes standards}
  \label{tab:organisation:classes}
  \begin{tabularx}{1.0\linewidth}{XXXXX}
    \toprule
    Classe & Divisions & Disposition & Entête & Pied de page \\
    \midrule
    \class{article} & parties,\newline sections, \dots
                       & recto & vide & folio centré \\
    \addlinespace[6pt]
    \class{report} & parties,\newline chapitres,\newline sections, \dots
                       & recto & vide & folio centré \\
    \addlinespace[6pt]
    \class{book} & parties,\newline chapitres,\newline sections, \dots
                       & recto verso & folio, titres & vide \\
    \bottomrule
  \end{tabularx}
\end{table}

\begin{figure}
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=1,width=0.95\linewidth]{auxdoc/exemple-classe-article}}
  \end{minipage}
  \hfill
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=2,width=0.95\linewidth]{auxdoc/exemple-classe-article}}
  \end{minipage}
  \caption{Exemple de mise en page avec la classe \class{article}}
  \label{fig:organisation:classes:article}
\end{figure}

\begin{figure}
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=1,width=0.95\linewidth]{auxdoc/exemple-classe-report}}
  \end{minipage}
  \hfill
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=2,width=0.95\linewidth]{auxdoc/exemple-classe-report}}
  \end{minipage}
  \caption{Exemple de mise en page avec la classe \class{report}}
  \label{fig:organisation:classes:report}
\end{figure}

\begin{figure}
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=1,width=0.95\linewidth]{auxdoc/exemple-classe-book}}
  \end{minipage}
  \hfill
  \begin{minipage}{0.49\linewidth}
    \fbox{\includegraphics[page=2,width=0.95\linewidth]{auxdoc/exemple-classe-book}}
  \end{minipage}
  \caption{Exemple de mise en page avec la classe \class{book}}
  \label{fig:organisation:classes:book}
\end{figure}

Cet ouvrage fait une large place à la classe qui est utilisée pour le
composer: la classe \class{memoir} \citep{memoir}. Il s'agit d'une
extension de la classe standard \class{book} qui facilite à plusieurs
égards la préparation de documents d'allure professionnelle dans
{\LaTeX}. Je recommande d'utiliser cette classe en lieu et place de la
classe \class{book}, ou même de la classe \class{article} (voir
ci-dessous).

La classe \class{memoir} incorpore d'office plus de 30 des paquetages
les plus populaires\footnote{%
  Consultez la section~18.24 de la documentation de \class{memoir}
  pour la liste ou encore le journal de la compilation (\emph{log})
  d'un document utilisant la classe.}. %
La classe fait partie des distributions {\LaTeX} modernes; elle
devrait être installée et disponible sur tout système. Elle est livrée
avec une %
\doc{memoir}{https://texdoc.net/pkg/memoir} %
exhaustive: le manuel d'instructions fait près de 600~pages! Il peut
être utile de s'y référer de temps à autre pour réaliser une mise en
page particulière.

Rappellons que l'on charge une classe de document au début du
préambule avec la commande
\begin{lstlisting}
\documentclass`\oarg{options}\marg{classe}'
\end{lstlisting}
Les \meta{options} disponibles varient d'une classe à l'autre. Les
plus courantes sont les suivantes.
\begin{description}
\item[\mdseries \code{10pt}, \code{11pt}, \code{12pt}] Taille de la
  police du document en points. La valeur par défaut est \code{10pt}.
  Je recommande d'utiliser plutôt \code{11pt}.
\item[\mdseries \code{oneside}, \code{twoside}] Disposition du
  document en recto seulement ou en recto verso. Ces options ne sont
  utiles que pour modifier la disposition par défaut de la classe.
\item[\mdseries \code{openright}, \code{openany}] Position de la
  première page des chapitres toujours à droite (page impaire) ou
  immédiatement après la dernière page du chapitre précédent. Avec la
  valeur par défaut, \code{openany}, {\LaTeX} insérera une page
  blanche dans le document si un chapitre se termine sur une page
  impaire.
\item[\mdseries \code{article} (classe \class{memoir} seulement)] Mise
  en page comme celle d'un article. Avec cette option, \class{memoir}
  peut remplacer la classe \class{article}, ce qui permet d'utiliser
  une seule et même classe pour les deux principaux types de document
  (article et livre).
\end{description}

D'autres options permettent de contrôler la position du titre, la
disposition en une ou deux colonnes, ou encore la position des
équations hors paragraphe. \citet{Thurnherr:class-options} offre une
présentation succincte des options standards. Le chapitre~1 de la
documentation de \class{memoir} traite en plus des ajouts propres à
cette classe.



\section{Parties d'un document}

Tout document de plus de quelques pages est normalement divisé en
chapitres, sections, sous-sections, etc. Il peut comporter une ou
plusieurs annexes et débuter par un résumé, notamment s'il s'agit d'un
article scientifique. Le document est habituellement coiffé d'un
titre, mais celui-ci est parfois affiché sur une page de titre
séparée.

Toutes ces considérations relevant essentiellement de la mise en page,
{\LaTeX} s'en charge pour vous. Vous n'avez qu'à spécifier la strucure
logique du document à l'aide des commandes de la présente section.

\subsection{Titre et page de titre}
\label{sec:organisation:parties:titre}

{\LaTeX} rend très simple la composition du titre d'un article
scientifique ou d'une page de titre simple (classes \class{report},
\class{book}, \class{memoir}).

En premier lieu, vous devez spécifier, habituellement dans le
préambule, le titre du document, le nom du ou des auteurs et la date
de publication avec les commandes suivantes:
\begin{lstlisting}
\title`\marg{Titre du document}'
\author`\marg{Prénom Nom {\pixbsbs} Affiliation {\pixbsbs} Adresse}'
\date`\marg{Date ou autre texte}'
\end{lstlisting}
Un long titre sera scindé automatiquement. Vous pouvez aussi scinder
le titre manuellement en insérant la commande «\pixbsbs» aux points de
coupure. %
Si l'ouvrage comporte deux auteurs ou plus, insérez les informations
dans la commande \cmd{\author} les unes après les autres en séparant
chaque entrée par la commande \cmdprint{\and}. %
La commande \cmd{\date} insère le texte donné en argument (qu'il
s'agisse d'une date ou non) à l'endroit prévu à cet effet par
{\LaTeX}. Si l'on omet la commande, {\LaTeX} insère la date du jour au
moment de la compilation. Pour ne pas afficher la date, laissez
simplement l'argument vide:
\begin{lstlisting}
\date{}
\end{lstlisting}

Dans les articles scientifiques, le nom d'un auteur est fréquemment
suivi d'un appel de note renvoyant à des remerciements à un organisme
subventionnaire ou à quelque autre information sur l'auteur. On insère
une telle note et son appel à l'endroit approprié dans les commandes
\cmd{\title} ou \cmd{\author} avec la commande \cmd{\thanks}:
\begin{lstlisting}
\thanks`\marg{Texte}'
\end{lstlisting}

Les commandes ci-dessus ne permettent que de saisir les informations
relatives au titre. Pour produire le titre il faut, en second lieu,
insérér dans le corps du document la commande \cmd{\maketitle}. La
commande insère le titre à l'endroit où elle apparait dans le code
source.

\begin{exemple}
  \label{ex:organisation:titre}
  Le code ci-dessous permet de créer un titre d'article standard. La
  page composée avec {\XeLaTeX} (puisque nous utilisons le paquetage
  \pkg{fontspec}) se trouve à la \autoref{fig:organisation:titre}.
  \begin{demo}
    \lstinputlisting[lastline=19]{auxdoc/exemple-titre.tex}
  \end{demo}

  La date de publication qui apparait dans la page composée est celle
  de la compilation puisque la commande \cmdprint{\date} n'apparait
  pas dans le code source.

  Remarquez également que j'ai placé un symbole de commentaire «\%»
  immédiatement après le nom de l'auteur dans le code source. Tel
  qu'expliqué à la \autoref{sec:bases:caracteres:espaces}, c'est pour
  éviter que {\LaTeX} ne transforme le retour à la ligne avant la
  commande \cmd{\thanks} en une espace entre le nom et l'appel de
  note.

  (Le paquetage \pkg{lorem} utilisé dans cet exemple permet
  d'insérer du faux texte \link{https://fr.lipsum.com}{Lorem Ipsum}
  dans un document \LaTeX.)
  \begin{figure}
    \centering
    \fbox{\includegraphics[width=0.7\linewidth]{auxdoc/exemple-titre}}
    \caption{Illustration d'un titre d'article standard.}
    \label{fig:organisation:titre}
  \end{figure}
  \qed
\end{exemple}

J'ai mentionné plus haut que {\LaTeX} peut aussi produire
automatiquement la page de titre d'un rapport ou d'un livre. Il est
toutefois peu probable qu'elle convienne, surtout dans le cas d'un
livre. Des options plus flexibles existent: les environnements
\Ie{titlepage} (classes standards) et \Ie{titlingpage} (classe
\class{memoir}). Ces environnements permettent de définir librement
une page de titre:
\begin{demo}
  \begin{minipage}{0.48\linewidth}
\begin{lstlisting}
\begin{titlepage}
  `\meta{Texte de la page de titre}'
\end{titlepage}
\end{lstlisting}
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\linewidth}
\begin{lstlisting}
\begin{titlingpage}
  `\meta{Texte de la page de titre}'
\end{titlingpage}
\end{lstlisting}
  \end{minipage}
\end{demo}
Vous contrôlez alors entièrement la disposition et la composition des
éléments de la page de titre. Consultez le chapitre~4 de la %
\doc{memoir}{https://texdoc.net/pkg/memoir} %
de \class{memoir} pour une liste de bonnes pratiques en matière de
composition de page de titre et pour des exemples détaillés.

\subsection{Résumé}
\label{sec:organisation:parties:resume}

Les articles scientifiques et les rapports comportent souvent un
résumé, habituellement composé en retrait des marges gauche et droite
et dans une police plus petite. Le résumé est produit avec
l'environnement \Ie{abstract} des classes \class{article},
\class{report} ou \class{memoir}:
\begin{lstlisting}
\begin{abstract}
  `\meta{Texte du résumé}'
\end{abstract}
\end{lstlisting}

\subsection{Sections}
\label{sec:organisation:parties:sections}

Vous pouvez diviser un document en sections
qui seront automatiquement numérotées par {\LaTeX} de manière
séquentielle avec les commandes suivantes:
\begin{lstlisting}
\part`\oarg{titre court}\marg{titre}'
\chapter`\oarg{titre court}\marg{titre}'
\section`\oarg{titre court}\marg{titre}'
\subsection`\oarg{titre court}\marg{titre}'
\subsubsection`\oarg{titre court}\marg{titre}'
\paragraph`\oarg{titre court}\marg{titre}'
\subparagraph`\oarg{titre court}\marg{titre}'
\end{lstlisting}
Les commandes forment, dans l'ordre ci-dessus, une hiérarchie des
titres d'un document\footnote{%
  Je n'ai jamais utilisé les niveaux de division \cmdprint{\paragraph}
  et \cmdprint{\subparagraph}.}. %
Tel que mentionné précédemment, la commande \cmd{\chapter} n'est pas
disponible avec la classe \class{article}.

Chaque commande prend en argument obligatoire le \meta{titre} de la
section. Si celui-ci est très long, il peut être utile de fournir en
argument optionnel un \meta{titre court}; c'est ce dernier qui
apparaitra dans la table des matières et dans les entêtes de page, le cas
échéant.

Toutes les commandes existent en version étoilée (nom suivi de
«\verb=*=») qui supprime la numérotation ainsi que l'insertion
éventuelle dans la table des matières (plus de détails à la
\autoref{sec:organisation:tdm}).

\tipbox{Évitez d'utiliser des sous-sous-sections numérotées (commande
  \cmdprint{\subsubsection}) dans un livre. Cela résulte en une
  numérotation à quatre niveaux qui s'avère difficile à suivre pour le
  lecteur.}

\subsection{Annexes}
\label{sec:organisation:parties:annexes}

Les annexes sont des sections ou des chapitres avec une numérotation
alphanumérique (A, A.1, ...) plutôt qu'entièrement numérique. On
informe {\LaTeX} que les sections suivantes doivent être traitées
comme des annexes en insérant dans le document la commande
%% [hack ci-dessous pour cacher la commande \appendix à reftex]
\begin{lstlisting}
`\verb=\appendix='
\end{lstlisting}
En plus de modifier le style de numérotation, la commande a pour effet
de changer le mot clé «Chapitre» pour «Annexe» dans les titres de
chapitre.

\subsection{Structure logique d'un livre}
\label{sec:oganisation:parties:livre}

Un livre se compose normalement de trois grandes parties logiques: les
pages liminaires (avant-propos, table des matières, tout ce qui
précède le chapitre premier); le corps du livre (chapitres et
annexes); les parties en fin d'ouvrage (bibliographie, index). Les
commandes
\begin{lstlisting}
\frontmatter
\mainmatter
\backmatter
\end{lstlisting}
qui sont disponibles avec les classes \class{book} et \class{memoir},
permettent d'identifier ces trois parties. Le
\autoref{tab:organisation:livre} résume l'effet de chaque commande.

\begin{table}
  \centering
  \caption{Commandes d'identification de la structure logique d'un
    livre et leurs effets}
  \label{tab:organisation:livre}
  \begin{tabular}{ll}
    \toprule
    Commande & Effets \\
    \midrule
    \cmd{\frontmatter} & numérotation des
                              pages en chiffres romains (i, ii, ...) \\
             & chapitres non numérotés \\
    \addlinespace[0.5\normalbaselineskip]
    \cmd{\mainmatter} & numérotation des pages
                             à partir de 1 en chiffres arabes \\
             & chapitres numérotés \\
    \addlinespace[0.5\normalbaselineskip]
    \cmd{\backmatter} & numérotation des pages
                             se poursuit \\
             & chapitres non numérotés \\
    \bottomrule
  \end{tabular}
\end{table}


\section{Table des matières}
\label{sec:organisation:tdm}

Dans la mesure où vous avez bien identifié les différentes divisions
d'un ouvrage avec les commandes mentionnées à la section précédente,
la production de la table des matières est on ne peut plus simple avec
{\LaTeX}: il suffit d'insérer la commande
\begin{lstlisting}
\tableofcontents
\end{lstlisting}
dans le corps du document à l'endroit où la table des matières doit
apparaitre. C'est tout!

Lorsque le paquetage \pkg{hyperref} \citep{hyperref} est chargé, la
commande \cmdprint{\tableofcontents} produit également la table des
matières du fichier PDF. Cela permet de naviguer dans le document
directement depuis la visionneuse PDF. La
\autoref{fig:organisation:tdm-dans-pdf} illustre cette fonctionnalité
avec la visionneuse Aperçu de macOS.

\begin{figure}
  \resizebox{\textwidth}{!}{\includegraphics{images/tdm-dans-pdf}}
  \caption[Consultation d'un document PDF avec la visionneuse Aperçu
  de macOS]{%
    Consultation d'un document PDF avec la visionneuse Aperçu
    de macOS. La barre latérale de gauche affiche la table des
    matières du fichier PDF, ce qui permet de naviguer dans le
    document sans devoir revenir à celle du document.}
  \label{fig:organisation:tdm-dans-pdf}
\end{figure}

\importantbox{La production initiale de la table des matières et la
  prise en compte de toute modification requiert jusqu'à trois
  compilations consécutives du document.}

Par défaut, une section non numérotée ne figure pas dans la table des
matières. Si l'y insérer néanmoins, vous devez utiliser la commande
suivante:
\begin{lstlisting}
\addtocontentsline{toc}`\marg{niveau}\marg{titre}'
\end{lstlisting}
où \meta{niveau} est le nom de la commande de division sans le
caractère {\bs} (\code{chapter}, \code{section}, etc.) et \meta{titre}
est le texte qui doit figurer dans la table des matières.

Outre une table des matières, les ouvrages scientifiques comportent
parfois une liste des figures et une liste des tableaux. {\LaTeX} les
produit automatiquement avec les commandes
\begin{lstlisting}
\listoffigures
\listoftables
\end{lstlisting}

Dans la classe \class{memoir}, les commandes ci-dessus insèrent leur
propre titre de section dans la table des matière (autrement dit, la
table des matières apparait dans la table des matières). Les versions
\begin{lstlisting}
\tableofcontents*
\listoffigures*
\listoftables*
\end{lstlisting}
adoptent le comportement des classes standards, soit d'omettre ces
parties dans la table des matières.


\section{Renvois automatiques}
\label{sec:organisation:renvois}

Un document d'une certaine ampleur contiendra souvent des renvois à
une section, un tableau, une équation, voire une page spécifique.
Évidemment, le numéro de la section, du tableau, de l'équation ou de
la page est susceptible de changer au fil de la rédaction du document.
C'est pourquoi vous ne devriez \emph{jamais} insérer les revois
manuellement dans le texte. C'est une tâche qu'il vaut mieux confier à
l'ordinateur.

\subsection{Étiquettes et renvois}
\label{sec:organisation:renvois:etiquettes}

Les renvois automatiques dans {\LaTeX} reposent sur un système
d'étiquettes attribuées à des éléments de contenu et de référencement
par ces étiquettes. Ainsi, pour effectuer un renvoi vers un élément de
contenu, il faut d'abord nommer celui-ci en insérant la commande
\begin{lstlisting}
\label`\marg{nom}'
\end{lstlisting}
à proximité de l'élément. Le choix du \meta{nom} est tout à fait libre. Il
peut être constitué de toute combinaison de lettres, de chiffres et de
symboles autres que les symboles réservés mentionnés à la
\autoref{sec:bases:caracteres:reserves}.

Ensuite, il devient possible d'insérer un renvoi à cet élément de contenu
n'importe où dans le document avec la commande
\begin{lstlisting}
\ref`\marg{nom}'
\end{lstlisting}
Pour renvoyer à la page où se trouve l'élément, utilisez la commande
\begin{lstlisting}
\pageref`\marg{nom}'
\end{lstlisting}

\begin{exemple}
  \label{ex:organisation:renvoi}
  Le code ci-dessous permet d'insérer un renvoi automatique à un
  numéro de section dans un document. Le résultat se trouve à la
  \autoref{fig:organisation:renvoi}.
\begin{lstlisting}[emph={\label,\ref}]
\section{Définitions}
\label{sec:definitions}

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Duis in auctor dui. Vestibulum ut, placerat ac, adipiscing
vitae, felis.

\section{Historique}

Tel que vu à la section \ref{sec:definitions}, on a...
\end{lstlisting}

  \begin{figure}
    \fbox{\includegraphics[viewport=124 550 484 664,clip=true,width=0.98\linewidth]{auxdoc/exemple-renvoi}}
    \caption{Texte produit par le code de
      l'\autoref{ex:organisation:renvoi} illustrant un renvoi
      automatique standard}
    \label{fig:organisation:renvoi}
  \end{figure}
  \qed
\end{exemple}

Un long document contiendra vraisemblablement un grand nombre
d'étiquettes et de renvois. Afin de vous y retrouver et pour réduire
les risques de doublons, adoptez une manière systématique et
mnémotechnique de nommer les éléments. Par exemple, pour le présent
document, j'ai utilisé un système d'étiquettes de la forme
suivante:
\begin{itemize}
\item \cmdprint{chap:\meta{chapitre}} pour les chapitres;
\item \cmdprint{sec:\meta{chapitre}:\meta{section}} pour les sections;
\item \cmdprint{tab:\meta{chapitre}:\meta{tableau}} pour les tableaux;
\item \cmdprint{eq:\meta{chapitre}:\meta{equation}} pour les
  équations.
\end{itemize}

\subsection{Production des renvois}
\label{sec:organisation:renvois:production}

La production des renvois requiert deux à trois compilations.
Tant que {\LaTeX} n'a pas complété les renvois, le journal de
compilation contient, vers la toute fin, le message
\begin{verbatim}
LaTeX Warning: Label(s) may have changed. Rerun to get
cross-references right.
\end{verbatim}

Prenez également garde aux alertes suivantes dans le journal de
compilation. Elles identifient des correctifs à apporter dans le
système d'étiquettes et de renvois. D'abord, le message
\begin{verbatim}
LaTeX Warning: There were undefined references.
\end{verbatim}
indique qu'une ou plusieurs commandes \cmd{\ref} renvoient à un
\meta{nom} qui n'est pas défini avec \cmd{\label}. Le document
contiendra alors le caractère \textbf{?} en lieu et place du renvoi. À
l'inverse, le message
\begin{verbatim}
LaTeX Warning: There were multiply-defined labels.
\end{verbatim}
indique que plusieurs commandes \cmdprint{\label} utilisent le même
\meta{nom}. Cela est susceptible de causer des renvois vers le mauvais
élément de contenu.

\subsection{Renvois avec hyperliens}
\label{sec:organisation:renvois:hyperliens}

Lorsque chargé dans le document, le paquetage \pkg{hyperref} insère
des hyperliens vers les renvois dans les fichiers PDF. C'est très
pratique en consultation électronique d'un document.

\begin{exemple}
  \label{ex:organisation:renvoi-hyperref}
  Avec l'ajout de
\begin{lstlisting}
\usepackage{hyperref}
\end{lstlisting}
  dans le préambule, le code de l'\autoref{ex:organisation:renvoi}
  produit maintenant le type de renvoi illustré à la
  \autoref{fig:organisation:renvoi-hyperref}. Le texte en couleur
  contrastante est un hyperlien vers le titre de la section.
  \begin{figure}
    \centering
    \fbox{\includegraphics[viewport=124 550 484 664,clip=true,width=0.98\linewidth]{auxdoc/exemple-renvoi-hyperref}}
    \caption{Texte produit par le code de
      l'\autoref{ex:organisation:renvoi} après l'ajout du paquetage
      \pkg{hyperref}}
    \label{fig:organisation:renvoi-hyperref}
  \end{figure}
  \qed
\end{exemple}

L'inconvénient avec la procédure illustrée dans l'exemple précédent,
c'est que seul le numéro de la référence est transformé en hyperlien.
La zone disponible pour cliquer s'en trouve plutôt restreinte. Une
fonctionnalité du paquetage \pkg{hyperref} --- à laquelle j'ai eu
recours dans le présent document --- permet d'agrandir cette zone. La
commande
\begin{lstlisting}
\autoref`\marg{nom}'
\end{lstlisting}
permet de nommer automatiquement le type de renvoi (section, équation,
tableau, etc.) et de transformer en hyperlien à la fois ce texte et le
numéro du renvoi.

\begin{exemple}
  \label{ex:organisation:renvoi-autoref}
  Reprenons le texte de l'\autoref{ex:organisation:renvoi}, mais en
  utilisant cette fois la commande \cmd{\autoref} pour insérer un
  renvoi dans le texte. Remarquez que le mot «section» a été supprimé
  du code source pour laisser à la commande le soin d'insérer
  l'étiquette appropriée dans le document.
\begin{lstlisting}[emph={\label,\autoref}]
\section{Définitions}
\label{sec:definitions}

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Duis in auctor dui. Vestibulum ut, placerat ac, adipiscing
vitae, felis.

\section{Historique}

Tel que vu à la \autoref{sec:definitions}, on a...
\end{lstlisting}
  \begin{figure}
    \centering
    \fbox{\includegraphics[viewport=124 550 484 664,clip=true,width=0.98\linewidth]{auxdoc/exemple-renvoi-autoref}}
    \caption{Texte produit par le code de
      l'\autoref{ex:organisation:renvoi-autoref}}
    \label{fig:organisation:renvoi-autoref}
  \end{figure}
  La \autoref{fig:organisation:renvoi-autoref} montre que le mot
  «section» ainsi que son numéro forment maintenant l'hyperlien.
  \qed
\end{exemple}

La \autoref{sec:trucs:hyperliens} fournit des détails
additionnels sur la gestion des hyperliens dans un document PDF.


\section{Document contenu dans plusieurs fichiers}
\label{sec:organisation:include}

Lorsque le préambule et le corps du texte demeurent relativement
courts (peu de commandes spéciales et moins d'une vingtaine de pages
de texte), il demeure assez simple et convivial d'en faire l'édition
dans un seul fichier à l'aide de votre éditeur de texte favori.

Cependant, si le préambule devient long et complexe ou, surtout,
lorsque l'ampleur du document augmente jusqu'à compter un grand nombre
de pages sur plusieurs chapitres, il convient de répartir les diverses
parties du document dans des fichiers séparés.

La segmentation en plusieurs fichiers rend l'édition du texte plus
simple et plus efficace. De plus, durant la phase de rédaction, elle
peut significativement accélérer la compilation des documents très
longs ou comptant plusieurs images.

\subsection{Insertion du contenu d'un autre fichier}
\label{sec:organisation:include:input}

La commande \cmd{\input} permet d'insérer le contenu d'un autre
fichier dans un document {\LaTeX}. La syntaxe de la commande est
\begin{lstlisting}
\input`\marg{fichier}'
\end{lstlisting}
où le nom du fichier à insérer est \meta{fichier}\code{.tex}.
L'extension \code{.tex} est donc implicite. Le contenu du fichier est
inséré tel quel dans le document, comme s'il avait été saisi dans le
fichier qui contient l'appel à \cmd{\input}.

Le procédé est surtout utile pour sauvegarder séparément des bouts de
code qui gênent l'édition du texte (figures, longs tableaux) ou qui
sont communs entre plusieurs documents (licence d'utilisation, auteurs
et affiliations).

La commande peut aussi être utilisée dans le préambule pour charger
une partie ou l'ensemble de celui-ci. Cela permet de composer un même
préambule pour plusieurs documents. Il suffit alors de faire
d'éventuelles modifications à un seul endroit pour les voir prendre
effet dans tous les documents.

\subsection{Insertion de parties d'un document}
\label{sec:organisation:include:include}

Je vous recommande de segmenter tout document d'une certaine ampleur
dans des fichiers \verb=.tex= distincts pour chaque partie ---
habituellement un fichier par chapitre. Vous composerez ensuite le
document complet à l'aide d'un fichier maitre qui contient le
préambule {\LaTeX} et un ensemble d'appels à la commande
\cmdprint{\include} pour réunir les parties dans un tout.

La syntaxe de \cmd{\include} est
\begin{lstlisting}
\include`\marg{fichier}'
\end{lstlisting}
où le nom du fichier à insérer est \meta{fichier}\code{.tex}. Ici
aussi l'extension \code{.tex} est implicite.

Comme \cmd{\input}, la commande \cmd{\include} insère le contenu d'un
autre fichier dans un document. Toutefois, l'insertion d'un fichier
avec \cmd{\include} débute toujours une nouvelle page. Cette commande
sert donc principalement pour insérer des chapitres entiers plutôt que
seulement des portions de texte. De plus, un fichier inséré avec
\cmd{\include} peut contenir des appels à \cmd{\input}, mais pas à
\cmd{\include}.

\begin{exemple}
  La \autoref{fig:organisation:maitre} présente un exemple type de
  fichier maitre qui fait appel à la commande \cmd{\include} pour
  composer un document chapitre par chapitre. %
  \qed

  \begin{figure}
    \centering
    \begin{minipage}{0.75\linewidth}
\begin{lstlisting}[numbers=left, numberstyle=\tiny]
\documentclass{memoir}
  [...]

\begin{document}

\frontmatter

\include{introduction}
\tableofcontents*

\mainmatter
\include{historique}
\include{rappels}
\include{modele}
[...]

\end{document}
\end{lstlisting}
    \end{minipage}
    \caption[Structure type d'un fichier maitre]{%
      Structure type d'un fichier maitre. Les fichiers
      \code{historique.tex}, \code{rappels.tex} et \code{modele.tex}
      contiennent le texte des trois premiers chapitres.}
    \label{fig:organisation:maitre}
  \end{figure}
\end{exemple}

\subsection{Compilation partielle}
\label{sec:organisation:include:compilation}

Le principal avantage de \cmd{\include} par rapport à \cmd{\input}
réside dans le fait que {\LaTeX} peut préserver entre les compilations
les informations telles que les numéros de pages, de sections ou
d'équations, ainsi que les renvois et les références bibliographiques.
Cela permet, par exemple, de compiler le texte d'un seul chapitre ---
plutôt que le document entier --- et de néanmoins obtenir une image
représentative du chapitre. Procéder ainsi accélère significativement
la compilation des documents longs ou complexes.

La commande \cmd{\includeonly}, qui est employée exclusivement dans le
préambule, sert à spécifier le ou les fichiers à compiler tout en
préservant la numérotation et les références. Sa syntaxe est
\begin{lstlisting}
\includeonly`\marg{liste\_fichiers}'
\end{lstlisting}
où \meta{liste\_fichiers} contient les noms des fichiers à
inclure dans la compilation, séparés par des virgules et sans
l'extension \code{.tex}.

Lors de l'utilisation de la commande \cmd{\includeonly}, toute la
numérotation dans les fichiers \meta{liste\_fichiers} suivra celle
établie lors de la compilation précédente. Si l'édition des fichiers
de \meta{liste\_fichiers} cause des changements dans la numérotation
et les références dans les autres parties du document, une nouvelle
compilation de l'ensemble ou d'une partie de celui-ci s'avérera
nécessaire.

\begin{exemple}
  Un document est composé en plusieurs parties avec les commandes
  suivantes:
\begin{lstlisting}
\include{historique}            % chapitre 1
\include{rappels}               % chapitre 2
\include{modele}                % chapitre 3
\end{lstlisting}
  Les chapitres débutent respectivement aux pages~1, 23 et 41.
  \begin{itemize}
  \item Si l'on ajoute au préambule du document la commande
\begin{lstlisting}
\includeonly{rappels}
\end{lstlisting}
    le numéro du chapitre sera toujours 2 et le folio de
    la première page sera toujours 23, même si les 22 pages
    précédentes ne se trouvent pas dans le document.
  \item Si l'on modifie le fichier \fichier{rappels.tex} de telle
    sorte que le chapitre se termine maintenant à la page 46, il
    faudra recompiler le document avec au moins les fichiers
    \fichier{rappels.tex} et \code{modele.tex} pour que les pages du
    chapitre~3 soient numérotées à partir de 47.
  \end{itemize}
  \qed
\end{exemple}

L'\autoref{ex:include} illustre mieux le cycle typique
d'utilisation des commandes \cmd{\include} et \cmd{\includeonly}.

\tipbox{Utilisez des noms de fichiers qui permettent de facilement
  identifier leur contenu. Par exemple, un nom comme
  \fichier{rappels.tex} identifie clairement le contenu du fichier et
  il résiste mieux aux changements à l'ordre des chapitres que
  \fichier{chapitre1.tex}.}

%%%
%%% Exercices
%%%

\section{Exercices}
\label{sec:organisation:exercices}

\Opensolutionfile{solutions}[solutions-organisation]

\begin{Filesave}{solutions}
\section*{Chapitre \ref*{chap:organisation}}
\addcontentsline{toc}{section}{Chapitre \protect\ref*{chap:organisation}}

\end{Filesave}

\begin{exercice}[nosol]
  Utiliser le fichier \fichier{exercice-parties.tex}.
  \begin{enumerate}
  \item Étudier la structure du document dans le code source.
  \item Ajouter un titre et un auteur au document à l'aide des
    commandes \cmdprint{\title} et \cmdprint{\author} se trouvant déjà
    dans le préambule.
  \item Créer la table des matières du document en le compilant deux à
    trois fois.
  \item Insérer deux ou trois titres de sections de différents niveaux
    dans le document et recompiler.
  \item La numérotation cesse à partir des sous-sections. C'est une
    particularité de la classe \class{memoir}. Recompiler le document
    après avoir ajouté au préambule la commande
\begin{lstlisting}
\maxsecnumdepth{subsection}
\end{lstlisting}
  \item Ajouter une annexe au document.
  \end{enumerate}
\end{exercice}

\begin{exercice}[nosol]
  Utiliser le fichier \fichier{exercice-renvois.tex}.
  \begin{enumerate}
  \item Insérer dans le texte un renvoi au numéro d'une section.
  \item Activer le paquetage \pkg{hyperref} avec l'option
    \code{colorlinks} et comparer l'effet d'utiliser \cmd{\ref} ou
    \cmd{\autoref} pour le renvoi.
  \end{enumerate}
\end{exercice}

\begin{exercice}[nosol]
  \label{ex:include}
  Cet exercice fait appel au fichier maitre
  \fichier{exercice-include.tex} et à plusieurs fichiers auxiliaires.
  Schématiquement, le document est composé ainsi:

  \medskip
  \begin{minipage}{\linewidth}
    \dirtree{%
      .1 exercice-include.tex.
      .2 {\textbackslash}input pagetitre.tex.
      .2 {\textbackslash}include rpresentation.tex.
      .3 {\textbackslash}includegraphics console-screenshot.pdf.
      .2 {\textbackslash}include emacs.tex.
    }
  \end{minipage}
  \medskip

  La commande \cmd{\includegraphics} permet d'insérer une image dans
  un document {\LaTeX}. Elle provient du paquetage \pkg{graphicx}.

  \begin{enumerate}
  \item Étudier le code source du fichier maitre, puis le compiler
    deux à trois fois jusqu'à ce que tous les renvois soient à jour.
    Il est normal à ce stade que la figure~1 du document soit vide.
  \item Ajouter dans le préambule du fichier maitre la commande
\begin{lstlisting}
\includeonly{emacs}
\end{lstlisting}
    puis compiler le document.

    Observer que, malgré l'absence du chapitre~1, la numérotation et
    les références demeurent à jour, notamment la table des matières.
  \item Remplacer la commande ajoutée en b) dans le préambule du
    fichier maitre par la commande
\begin{lstlisting}
\includeonly{rpresentation}
\end{lstlisting}
    Vers la fin du fichier \fichier{rpresentation.tex}, activer la
    commande
\begin{lstlisting}
\includegraphics[width=\textwidth]{console-screenshot}
\end{lstlisting}
    en supprimant le symbole «\%» au début de la ligne. Compiler de
    nouveau le document deux fois.

    Les modifications ont eu pour effet d'ajouter une page au
    chapitre~1. Observer que, selon la table des matières, le
    chapitre~2 débute toujours à la page~3 alors que celle-ci est
    maintenant occupée par la figure~1.
  \item Afin de corriger la table des matières, désactiver dans le
    préambule du fichier maitre la commande \cmd{\includeonly}, puis
    compiler de nouveau le document quelques fois.
  \end{enumerate}
\end{exercice}

\begin{exercice}[nosol]
  Déplacer dans un fichier \fichier{preambule.tex} toutes les lignes
  du préambule du fichier \fichier{exercice-include.tex} utilisé à
  l'exercice précédent, à l'exception de celles relatives à la page
  titre (titre, auteur, date). Insérer le préambule dans
  \fichier{exercice-include.tex} avec la commande \cmd{\input}.
\end{exercice}

\Closesolutionfile{solutions}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "formation-latex-ul"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
